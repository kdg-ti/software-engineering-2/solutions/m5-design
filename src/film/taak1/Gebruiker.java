package film.taak1;

public class Gebruiker {
    private String Naam;

    public Gebruiker(String naam) {
        Naam = naam;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Gebruiker{");
        sb.append("Naam='").append(Naam).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
