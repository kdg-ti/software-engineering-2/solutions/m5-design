package film.taak1;

import java.time.LocalDateTime;

public class Vertoning {
    private Film film;
    private LocalDateTime tijdstip;

    public Vertoning(Film film, LocalDateTime tijdstip) {
        this.film = film;
        this.tijdstip = tijdstip;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Vertoning{");
        sb.append("film=").append(film);
        sb.append(", tijdstip=").append(tijdstip);
        sb.append('}');
        return sb.toString();
    }
}
