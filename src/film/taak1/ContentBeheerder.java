package film.taak1;

import java.util.ArrayList;
import java.util.List;

public class ContentBeheerder extends  Gebruiker{
    private List<Film> filmsInBeheer= new ArrayList<>();

    public ContentBeheerder(String naam) {
        super(naam);
    }

    public void addFilm(Film f){
        filmsInBeheer.add(f);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ContentBeheerder{");
        sb.append(super.toString());
        sb.append("filmsInBeheer=").append(filmsInBeheer);
        sb.append('}');
        return sb.toString();
    }
}
