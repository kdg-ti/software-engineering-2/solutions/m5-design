package film.taak1;

import java.time.LocalDateTime;

public class Runner {
    public static void main(String[] args) {
        Film lionKing = new Film("The Lion King");
        Film starwars = new Film("Star Wars CXVI: The empire goes to Disneyworld");
        Film tiktak2 = new Film("TikTak2");

        Vertoning vertLionKing = new Vertoning(lionKing, LocalDateTime.now().withHour(16));
        Vertoning vertStarWars = new Vertoning(starwars, LocalDateTime.now().withHour(20));

        ProgrammaManager josAsPM = new ProgrammaManager("Jos as PM");
        josAsPM.addVertoning(vertLionKing);
        josAsPM.addVertoning(vertStarWars);


        ContentBeheerder josAsCM = new ContentBeheerder("Jos as CM");
        josAsCM.addFilm(tiktak2);

        System.out.println(josAsPM);
        System.out.println(josAsCM);

    }
}
