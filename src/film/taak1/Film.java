package film.taak1;

public class Film {
    private String naam;
    private String omschrijving;

    public Film(String naam) {
        this.naam = naam;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Film{");
        sb.append("naam='").append(naam).append('\'');
        sb.append(", omschrijving='").append(omschrijving).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
