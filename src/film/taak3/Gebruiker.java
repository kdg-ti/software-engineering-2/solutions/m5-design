package film.taak3;

public class Gebruiker extends RoleOwner {
    private String Naam;

    public Gebruiker(String naam) {
        Naam = naam;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Gebruiker{");
        sb.append("Naam='").append(Naam).append('\'');
        sb.append(", roles=").append(roles);
        sb.append('}');
        return sb.toString();
    }
}
