package film.taak3;

import java.util.ArrayList;
import java.util.List;

public abstract class RoleOwner {
    List<Role> roles = new ArrayList();

    public void addRole(Role r){
        roles.add(r);

    }

    public void removeRole (Role r){
        roles.remove(r);

    }

}
