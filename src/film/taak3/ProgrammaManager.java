package film.taak3;

import java.util.ArrayList;
import java.util.List;

public class ProgrammaManager extends Role{
    private List<Vertoning> vertoningenInBeheer = new ArrayList<>();

    public ProgrammaManager(RoleOwner owner) {
        super(owner);
    }


    public void addVertoning(Vertoning v){
        vertoningenInBeheer.add(v);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProgrammaManager{");
        sb.append(super.toString());
        sb.append("vertoningenInBeheer=").append(vertoningenInBeheer);
        sb.append('}');
        return sb.toString();
    }
}
