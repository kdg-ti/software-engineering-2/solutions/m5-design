package film.taak3;


import java.util.ArrayList;
import java.util.List;

public class ContentBeheerder extends  Role{
    private List<Film> filmsInBeheer= new ArrayList<>();

    public ContentBeheerder(RoleOwner owner) {
        super(owner);
    }


    public void addFilm(Film f){
        filmsInBeheer.add(f);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ContentBeheerder{");
        sb.append(super.toString());
        sb.append("filmsInBeheer=").append(filmsInBeheer);
        sb.append('}');
        return sb.toString();
    }
}
