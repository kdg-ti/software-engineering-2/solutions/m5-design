package film.taak3;

public abstract class Role {
    private RoleOwner owner;

    public Role(RoleOwner owner) {
        this.owner = owner;
    }
}
