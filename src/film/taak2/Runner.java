package film.taak2;

import java.time.LocalDateTime;

public class Runner {
    public static void main(String[] args) {
        Film lionKing = new Film("The Lion King");
        Film starwars = new Film("Star Wars CXVI: The empire goes to Disneyworld");
        Film tiktak2 = new Film("TikTak2");

        Vertoning vertLionKing = new Vertoning(lionKing, LocalDateTime.now().withHour(16));
        Vertoning vertStarWars = new Vertoning(starwars, LocalDateTime.now().withHour(20));

        Gebruiker jos = new Gebruiker("Jos ");
        jos.addVertoning(vertLionKing);
        jos.addVertoning(vertStarWars);
        jos.addFilm(tiktak2);

        System.out.println(jos);


    }
}
