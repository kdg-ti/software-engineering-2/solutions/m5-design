package film.taak2;

import java.util.ArrayList;
import java.util.List;

public class Gebruiker {
    private String Naam;
    private List<Film> filmsInBeheer= new ArrayList<>();
    private List<Vertoning> vertoningenInBeheer = new ArrayList<>();


    public Gebruiker(String naam) {
        Naam = naam;
    }

    public void addVertoning(Vertoning v){
        vertoningenInBeheer.add(v);
    }

    public void addFilm(Film f){
        filmsInBeheer.add(f);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Gebruiker{");
        sb.append("Naam='").append(Naam).append('\'');
        sb.append(", filmsInBeheer=").append(filmsInBeheer);
        sb.append(", vertoningenInBeheer=").append(vertoningenInBeheer);
        sb.append('}');
        return sb.toString();
    }
}
